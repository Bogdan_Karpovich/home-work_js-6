//1
const product = {
   name: 'Лампа',
   price: 100,
   discount: 20, 

   getDiscountedPrice() {
       const discountAmount = (this.price * this.discount) / 100;
       const finalPrice = this.price - discountAmount;
       return finalPrice;
   }
};
console.log(`Ціна товару '${product.name}' з урахуванням знижки: ${product.getDiscountedPrice()}`);

//2
function greeting(person) {
   return `Привіт, мені ${person.age} років. Моє ім'я - ${person.name}.`;
}

const name = prompt("Введіть своє ім'я:");
const age = parseInt(prompt("Введіть свій вік:"));

const message = greeting({ name, age });
alert(message);

//3
function deepClone(obj) {
   if (obj === null || typeof obj !== 'object') {
       return obj;
   }

   if (Array.isArray(obj)) { // Якщо це масив
       return obj.map(item => deepClone(item)); 
   }

   const clonedObj = {};
   for (const key in obj) {
       if (obj.hasOwnProperty(key)) {
           clonedObj[key] = deepClone(obj[key]);
       }
   }
   
   return clonedObj;
}

const originalObj = {
   name: "Igor",
   age: 20,
   details: {
       hobbies: ["reading", "gaming"],
       address: {
           city: "Kyiv",
           country: "Ukraine"
       }
   }
};

const clonedObj = deepClone(originalObj);

console.log("Original Object:", originalObj);
console.log("Cloned Object:", clonedObj);